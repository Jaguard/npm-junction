:: -------------------------------------------------------------------------------------------------------------------------------
:: Unlink the junction/symlink for the current directory from the NPM global prefix [1] or a remove a local link for the specified module [2]
:: Usage:
:: [1] nunlink - delete the module symlink for the <current directory> from the NPM global prefix
:: [2] nunlink module - delete the symlink from <current directory>/node_modules/<module>
:: -------------------------------------------------------------------------------------------------------------------------------
@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET NPM_MODULES=%APPDATA%\npm\node_modules
SET JUNCTION=%~dp0junction.exe
SET CWD=%CD%
:: create the NPM modules if not found
IF NOT EXIST "%JUNCTION%" (
	ECHO junction.exe not found in: %~dp0
	SET ERRORLEVEL=-1
	GOTO :EOF
)
:: check if a module is defined as first param
IF NOT "%1"=="" GOTO :LOCAL
:: -----------------------------
:: [1] delete the module symlink for the current directory from the NPM global prefix
:: -----------------------------
IF NOT EXIST "package.json" (
	:: get module name=parent directory
	FOR /D %%I IN ("%CWD%") DO SET MODULE=%%~nxI
) ELSE (
	:: get module from package.json
	FOR /F %%I IN ('node -p "require('./package.json').name"') DO SET MODULE=%%~nxI
)
SET GLOBAL_LINK=%NPM_MODULES%\%MODULE%
:: remove the previous link if exists
IF EXIST "%GLOBAL_LINK%" (
	:: try to remove the link with junction, otherwise remove as dir
	"%JUNCTION%" -d "%GLOBAL_LINK%" > nul
	IF EXIST "%GLOBAL_LINK%" (
		ECHO %GLOBAL_LINK% is not a symlink/junction!
  ) ELSE (
		ECHO Unlink [%MODULE%@%CWD%] ^<= %GLOBAL_LINK%
  ) 
) ELSE (
	ECHO [%MODULE%] is not a global module =^> %GLOBAL_LINK% ^(?^)
)
GOTO :EOF

:: -----------------------------
:: [2] delete the symlink from <current directory>/node_modules/<module>
:: -----------------------------
:LOCAL
:: get the module name from param
SET MODULE=%1
SET LOCAL_LINK=%CWD%\node_modules\%MODULE%
:: remove the local link if exists
IF EXIST "%LOCAL_LINK%" (
	:: try to remove the link with junction, otherwise remove as dir
	"%JUNCTION%" -d "%LOCAL_LINK%" > nul
	IF EXIST "%LOCAL_LINK%" (
		ECHO %LOCAL_LINK% is not a symlink/junction, remove as dir!
		rmdir /q /s "%LOCAL_LINK%"
	)
	ECHO Unlink [%MODULE%] ^<= %LOCAL_LINK%
) ELSE (
	ECHO [%MODULE%] is not a local module =^> %LOCAL_LINK% ^(?^)
)
GOTO :EOF