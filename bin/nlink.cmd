:: -------------------------------------------------------------------------------------------------------------------------------
:: Create a junction/symlink for the current directory/module into the NPM global prefix [1] or a reverse link for the specified module [2]
:: Usage:
:: [1] nlink        - create a symlink for the current directory under NPM global prefix
:: [2] nlink module - create a symlink into current dir/node_modules/<module> from the <module> link in NPM global prefix
:: -------------------------------------------------------------------------------------------------------------------------------
@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET NPM_MODULES=%APPDATA%\npm\node_modules
SET JUNCTION=%~dp0junction.exe
SET CWD=%CD%
:: create the NPM modules if not found
IF NOT EXIST "%JUNCTION%" (
	ECHO junction.exe not found in: %~dp0
	SET ERRORLEVEL=-1
	GOTO :EOF
)
:: check if a module is defined as first param
IF NOT "%1"=="" GOTO :REVERSE
:: -----------------------------
:: [1] create a symlink for the current directory/module under NPM global prefix
:: -----------------------------
IF NOT EXIST "package.json" (
	:: get module name=parent directory
	FOR /D %%I IN ("%CWD%") DO SET MODULE=%%~nxI
) ELSE (
	:: get module from package.json
	FOR /F %%I IN ('node -p "require('./package.json').name"') DO SET MODULE=%%~nxI
)
SET GLOBAL_LINK=%NPM_MODULES%\%MODULE%
:: remove the previous link if exists
IF EXIST "%GLOBAL_LINK%" (
	"%JUNCTION%" -d "%GLOBAL_LINK%" > nul
	IF EXIST "%GLOBAL_LINK%" (
		rmdir /q /s "%GLOBAL_LINK%"
	)
)
:: create the global NPM modules if not create yet
IF NOT EXIST "%NPM_MODULES%" (
	mkdir "%NPM_MODULES%"
)
:: create the direct link
"%JUNCTION%" "%GLOBAL_LINK%" "%CWD%" > nul
ECHO Link [%MODULE%@%CWD%] =^> %GLOBAL_LINK% 
GOTO :EOF

:: -----------------------------
:: [2] create a symlink for the current directory under NPM global prefix
:: -----------------------------
:REVERSE
:: get the module name from param
SET MODULE=%1
SET GLOBAL_LINK=%NPM_MODULES%\%MODULE%
:: create the NPM modules if not found
IF NOT EXIST "%GLOBAL_LINK%" (
	ECHO [%MODULE%] is not globally registered; %GLOBAL_LINK% not found!
	SET ERRORLEVEL=-2
	GOTO :EOF
)
SET LOCAL_MODULES=%CWD%\node_modules
:: create the global NPM modules if not create yet
IF NOT EXIST "%LOCAL_MODULES%" (
	mkdir "%LOCAL_MODULES%"
)
SET LOCAL_LINK=%LOCAL_MODULES%\%MODULE%
:: delete local link if exists already
IF EXIST "%LOCAL_LINK%" (
	"%JUNCTION%" -d "%LOCAL_LINK%" > nul
	IF EXIST "%LOCAL_LINK%" (
		rmdir /q /s "%LOCAL_LINK%"
	)
	ECHO Unlink existing [%MODULE%] ^<= %LOCAL_LINK%
)
:: create the reverse symlink
"%JUNCTION%" "%LOCAL_LINK%" "%GLOBAL_LINK%" > nul
ECHO Re-link [%MODULE%@%LOCAL_LINK%] =^> %GLOBAL_LINK%
GOTO :EOF