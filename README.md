# npm-junction
Alternative [NPM](https://npmjs.org/doc/cli/npm.html) [link](https://npmjs.org/doc/cli/npm-link.html)/[unlink](https://npmjs.org/doc/cli/npm-link.html) command line interface (CLI) scripts for NPM unsupported Windows (XP,2000,2003) using [SysInternals Junction](http://technet.microsoft.com/en-us/sysinternals/bb896768.aspx).

## Getting started
Install **`npm-junction`** globally and you'll have access to the **`nlink`** & **`nunlink`** commands anywhere on your Windows system.

```shell
npm install -g npm-junction
```
## **`nlink`** command/script 
Creates a junction/symlink for the current directory or module (if a **`package.json`** exists) into the NPM global prefix [1] or a reverse link for the specified module [2]

*Usage*:
--------
- **[1]** nlink - create a symlink for the **`[current directory]`** or module (if a **`package.json`** exists) under NPM global prefix
- **[2]** nlink module - create a symlink into **`[current directory]/node_modules/[module]`** from the [module] link in NPM global prefix

## **`nunlink`** command/script
Unlink the junction/symlink for the current directory or module (if a **`package.json`** exists) from the NPM global prefix [1] or a remove a local link for the specified module [2]

*Usage*:
--------
- **[1]** nunlink - delete the module symlink for the **`[current directory]`** or module (if a **`package.json`** exists) from the NPM global prefix
- **[2]** nunlink module - delete the symlink from **`[current directory]/node_modules/[module]`**

## **`nlink`** & **`nunlink`** commands/scripts usage in modules **`package.json`** script/hooks
```javascript
    // ..
	"scripts": {
		"postinstall": "nlink", // link current module into NPM global prefix
    "preinstall": "nunlink module", // unlink the specified global-linked module from the current module (to avoid npm install issues)
		"postinstall": "nlink module" // link the specified global-linked module from the current module
	},
    // ..
```

## npm-junction changelog

- v1.0.0 (2013.10.13)
 + Add current module name extraction from package.json
 * Update documentation with package.json script/hooks linking/unlinking

- v0.0.1 (2013.05.03)
 + Initial commit