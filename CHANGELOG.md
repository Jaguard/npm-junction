npm-junction changelog
======================

v1.0.0 (2013.10.13)
-------------------
+ Add current module name extraction from package.json
* Update documentation with package.json script/hooks linking/unlinking

v0.0.1 (2013.05.03)
-------------------
+ Initial commit